var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('scripts', function() {
    return gulp.src('src/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    gulp.watch('src/*.js', ['scripts']);
});

gulp.task('build', ['scripts', 'watch']);