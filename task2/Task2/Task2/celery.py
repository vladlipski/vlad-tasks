# -*- coding: utf-8 -*-
from __future__ import absolute_import
from django.conf import settings
import os

from celery import Celery
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Task2.settings')

app = Celery('Task2')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
)
if __name__ == '__main__':
    app.start()