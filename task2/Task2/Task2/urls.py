from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^audio/(?P<owner_id>[0-9]+)$', views.load_audio, name='load_audio'),
    url(r'^download/(?P<owner_id>[0-9]+)$', views.download_audio, name='download_audio'),
]
