from django.db import models

class AudioArchive(models.Model):
    owner_id = models.PositiveIntegerField()
    state = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.owner_id, self.state)

