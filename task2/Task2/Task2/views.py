# -*- coding: utf-8 -*-
import json
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse, FileResponse
from django.shortcuts import render
from .tasks import make_zip
from .models import AudioArchive
from wsgiref.util import FileWrapper
from django.templatetags.static import static

def index(request):
    return render(request, 'Task2/index.html', {})


def load_audio(request, owner_id):
	obj, created = AudioArchive.objects.get_or_create(owner_id=owner_id)
	if created:
		print ("created!")
		obj.save()
		make_zip.delay(owner_id, 5)

	response = {'state': obj.state}

	if obj.state:
		url = 'localhost:8000/download/%s' % (obj.owner_id)
		response.update({'url': url})

	return JsonResponse(response)

def download_audio(request, owner_id):	
	f = open(static('%s.zip' % (owner_id)), "rb")

	response = HttpResponse(FileWrapper(f), content_type='application/zip')
	response['Content-Disposition'] = 'attachment; filename=%s_audio.zip' % (owner_id)

	return response
