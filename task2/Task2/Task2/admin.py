from django.contrib import admin

from .models import AudioArchive

admin.site.register(AudioArchive)