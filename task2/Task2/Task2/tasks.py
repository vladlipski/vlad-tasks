# -*- coding: utf-8 -*-
from __future__ import absolute_import
from .audio_loader import AudioLoader
from Task2.celery import app
from .models import AudioArchive

@app.task()
def make_zip(owner_id, count):
    loader = AudioLoader(owner_id, count)
    loader.get_audio()
    file = AudioArchive.objects.get(owner_id=owner_id)
    file.state = True
    file.save()