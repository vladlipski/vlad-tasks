function downloadAudio() {
	var id = $('#inputId').val();
	var audio_url = 'audio/' + id;
	var download_url = 'download/' + id;

	$('#downloadButton').html('Wait please...');
	var interval = window.setInterval(function(){
		$.get(audio_url, function(data, status){
			if (data['state'] == true) {
				clearInterval(interval);
				window.location.replace(download_url);
				$('#downloadButton').html('Download');
			}
		}, "json");
	}, 3000);
}