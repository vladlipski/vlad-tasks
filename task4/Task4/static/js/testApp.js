var app = angular.module('testApp', []);

app.controller('testCtrl', function($scope) {
	$scope.$watch('enteredText', function(newValue, oldValue) {
  		if (newValue != undefined && newValue != "") {
	  		$scope.editedText = "You wrote " + newValue;
  		}
  		else{
			$scope.editedText = ""
  		}
	})
});