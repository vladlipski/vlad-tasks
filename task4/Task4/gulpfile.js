var gulp = require('gulp');

gulp.task('vendor', function() {
    return gulp.src('bower_components/angular/angular.js')
        .pipe(gulp.dest('static/vendor'));
});

gulp.task('build', ['vendor']);